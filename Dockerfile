FROM gitlab-registry.cern.ch/linuxsupport/alma9-base

WORKDIR /app
COPY parsedmarc/parsedmarc parsedmarc/
COPY parsedmarc/README.md parsedmarc/pyproject.toml ./

# Install dependencies
RUN dnf -y install \
  pip python3-gobject gobject-introspection libsecret \
  patch

# Build
RUN pip install -U pip
RUN pip install hatch
RUN hatch build
RUN pip install dist/*.whl

# Prevent python output buffer so we see properly MS Device login information
ENV PYTHONUNBUFFERED=1

ENTRYPOINT ["parsedmarc", "-c", "parsedmarc.ini"]
