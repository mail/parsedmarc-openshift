# parsedmarc-openshift

### parsedmarc 

- Source: https://github.com/domainaware/parsedmarc/
- Documentation: https://domainaware.github.io/parsedmarc/splunk.html

An Opensearch/Elasticsearch index prefix is needed. A Pull request with a fix was provided:
- https://github.com/domainaware/parsedmarc/pull/531

Until then, use the fixed version here (static copy in this repo):
- https://github.com/nopap/parsedmarc/tree/prefix_support

When pull request will be merged, replace the static copy by a github project link.


### Manually build image and deploy
Image is built and pushed using gitlab-ci, but you can manually test and push

Build the image:
```
podman build -t gitlab-registry.cern.ch/mail/parsedmarc-openshift .
```

Test locally the image:
```
podman-compose up
```

Push it to CERN Gitlab registry:
```
podman login gitlab-registry.cern.ch
podman push gitlab-registry.cern.ch/mail/parsedmarc-openshift
```

### Config files and tokens
Check `parsedmarc.ini.sample` for details.

Required:
```
nameservers = CERN DNS IPs comma separated
always_use_local_files = True
...
index_prefix = uc1-
```

We need to access the needed mailbox from a normal workstation
and get two files:

* The token config, usually in the current location under `.token`
* The actual secret token, usually in `$HOME/.IdentityService/parsedmarc`

To obtain those, we need to create a config file that only accesses the
Exchange Online mailbox and run the container. Example of config file:

```
[msgraph]
auth_method = DeviceCode
tenant_id = INSERT_TENANT_IT
client_id = INSERT_CLIENT_IT
client_secret = # leave empty
user = mailbox.primary.address@cern.ch
allow_unencrypted_storage = True
```

and then run the container interactively (`-it`) with eg:

```
podman run -it \
  --volume $PWD/parsedmarc.ini:/app/parsedmarc.ini \
  --volume $PWD/token:/app/token \
  --volume $HOME/.IdentityService/:/root/.IdentityService/ \
  gitlab-registry.cern.ch/mail/parsedmarc-openshift:latest
```

To ensure to see the output, add this ENV so python flushes all output:
```
PYTHONUNBUFFERED: 1
```

This should output something like that:

```
To sign in, use a web browser to open the page https://microsoft.com/devicelogin and enter the code XXXX to authenticate.
```

We visit the page, enter the code, login with the desired mailbox and
we obtain the needed tokens.

Now we can add the `parsedmarc` configuration file as a secret as it
contains passwords:

```
oc create secret generic parsedmarc.ini --from-file=/path/to/parsedmarc.ini
```

We add the token storage location as a PVCs as `parsedmarc` needs to
write on them upon token refresh. Follow the instructions over [at the
OKD configuration
guide](https://paas.docs.cern.ch/3._Storage/persistent-storage/), 100MB
size is more than enough. We set the names as `token` and
`identityservice`.

Finally, we edit the deployment and add the relevant volumes and mount points:

```
oc edit deployment
...
 template:
    spec:
      containers:
      - image: [...]
        volumeMounts:
        - mountPath: /app
          name: parsedmarc-ini
        - mountPath: /.IdentityService
          name: identityservice
        - mountPath: /app/token
          name: token
      volumes:
      - name: parsedmarc-ini
        secret:
          secretName: parsedmarc.ini
      - name: identityservice
        persistentVolumeClaim:
          claimName: identityservice
      - name: token
        persistentVolumeClaim:
          claimName: token
```

Once the deployment runs, we need to copy the content of our `.token`
file to `/app/token/.token`, and of
`/path/to/.IdentityService/parsedmarc` to `/.IdentityService/parsedmarc`
.

## Operations

### Image update

For example when updating the `parsedmarc` submodule, the `gitlab-ci.yml` will take care of rebuilding the image and push it to the registry.

In order to have the image deployed we need to push it to Openshift that
will trigger a new roll-out (and therefore pod re-creation):

```
oc tag gitlab-registry.cern.ch/mail/parsedmarc-openshift:latest parsedmarc-openshift:latest
```

### Trigger a roll-out

This is for example when we change the `parsedmarc.ini` config file.

```
oc rollout restart deployment parsedmarc-openshift
```

### Update `parsedmarc` configuration

Note: this will not trigger a roll-out ie changes won't be picked up
automatically.

```
oc delete secret parsedmarc.ini && \
oc create secret generic parsedmarc.ini --from-file=/path/to/parsedmarc.ini
```

### Refresh token

It is sometimes needed to refresh the token. We use the same interactive technique as above:

For this we need to access the needed mailbox from a normal workstation
and get two files:

* The token config, usually in the current location under `.token`
* The actual secret token, usually in `$HOME/.IdentityService/parsedmarc`

To obtain those, we need to create a config file that only accesses the
Exchange Online mailbox and run the container. Example of config file:

```
[msgraph]
auth_method = DeviceCode
tenant_id = INSERT_TENANT_IT
client_id = INSERT_CLIENT_IT
client_secret = # leave empty
user = mailbox.primary.address@cern.ch
allow_unencrypted_storage = True
```

and then run the container interactively (`-it`) with eg:

```
podman run -it \
  --volume $PWD/parsedmarc.ini:/app/parsedmarc.ini \
  --volume $PWD/token:/app/token \
  --volume $HOME/.IdentityService/:/root/.IdentityService/ \
  gitlab-registry.cern.ch/mail/parsedmarc-openshift:latest
```

This should output something like that:

```
To sign in, use a web browser to open the page https://microsoft.com/devicelogin and enter the code XXXX to authenticate.
```

We visit the page, enter the code, login with the desired mailbox and
we obtain the needed tokens.

Once we have them, we copy them to the running container and trigger a rollout:

```
$ oc get pods
NAME                                    READY   STATUS    RESTARTS      AGE
parsedmarc-openshift-xxxxxxxxxx-yyyyy   1/1     Running   3 (10m ago)   30m
$ oc cp token/.token parsedmarc-openshift-xxxxxxxxxx-yyyyy:/app/token/.token
$ oc cp ~/.IdentityService/parsedmarc.nocae parsedmarc-openshift-xxxxxxxxxx-yyyyy:/.IdentityService/
$ oc rollout restart deployment parsedmarc-openshift
```
